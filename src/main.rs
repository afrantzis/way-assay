//! This is provided as a debug tool for testsuite integration.

use std::{env, path::PathBuf};

use crate::dlopen::DlError;
use anyhow::Context;
use bpaf::Bpaf;
pub use libloading::{Error as LibLoadingError, Library, Symbol};

use tracing_subscriber::filter::EnvFilter;
mod dlopen;

mod test_harness;
#[cfg(feature = "wlcs")]
mod wlcs;

fn setup_tracing() -> anyhow::Result<()> {
    let subscriber = tracing_subscriber::fmt()
        .pretty()
        .with_env_filter(EnvFilter::from_default_env())
        .finish();

    tracing::subscriber::set_global_default(subscriber)
        .context("Couldn't set up tracing subscriber")
}

#[derive(Bpaf)]
#[bpaf(options, version)]
pub struct Arguments {
    #[bpaf(positional("LIBRARY"))]
    /// Path to the Wayland test harness (.so)
    path: PathBuf,
}

fn main() -> anyhow::Result<()> {
    setup_tracing()?;

    let args = arguments().run();
    let path = args.path.to_str().context("Couldn't obtain {path}")?;

    #[cfg(feature = "wlcs")]
    wlcs::WlcsConnection::new(path)?;

    Ok(())
}
