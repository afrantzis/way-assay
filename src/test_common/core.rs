//! Handlers for Wayland Core protocol events.
//!
//! TODO: Add SCTK delegates for higher level usage

const SHM_POOL_SIZE: i32 = 32_000_000;

use memfile::{CreateOptions, MemFile};
use wayland_client::{
    protocol::{wl_buffer, wl_compositor, wl_shm, wl_shm_pool, wl_surface},
    Dispatch,
};

use super::WaylandTest;

#[derive(Debug, Default)]
pub struct WaylandCoreState {
    pub(crate) compositor: Option<wl_compositor::WlCompositor>,
    pub(crate) shm_pool: Option<wl_shm_pool::WlShmPool>,
    pub(crate) shm: Option<wl_shm::WlShm>,
    pub(crate) buffer: Option<wl_buffer::WlBuffer>,
}

impl Dispatch<wl_compositor::WlCompositor, ()> for WaylandTest {
    fn event(
        state: &mut Self,
        _proxy: &wl_compositor::WlCompositor,
        event: <wl_compositor::WlCompositor as wayland_client::Proxy>::Event,
        _data: &(),
        _conn: &wayland_client::Connection,
        _qhandle: &wayland_client::QueueHandle<Self>,
    ) {
        state
            .core_callbacks
            .compositor_callback(&mut state.test_data, event)
    }
}

impl Dispatch<wl_shm_pool::WlShmPool, ()> for WaylandTest {
    fn event(
        state: &mut Self,
        _proxy: &wl_shm_pool::WlShmPool,
        event: <wl_shm_pool::WlShmPool as wayland_client::Proxy>::Event,
        _data: &(),
        _conn: &wayland_client::Connection,
        _qhandle: &wayland_client::QueueHandle<Self>,
    ) {
        state
            .core_callbacks
            .shm_pool_callback(&mut state.test_data, event)
    }
}

impl Dispatch<wl_shm::WlShm, ()> for WaylandTest {
    fn event(
        state: &mut Self,
        _proxy: &wl_shm::WlShm,
        event: <wl_shm::WlShm as wayland_client::Proxy>::Event,
        _data: &(),
        _conn: &wayland_client::Connection,
        _qhandle: &wayland_client::QueueHandle<Self>,
    ) {
        state
            .core_callbacks
            .shm_callback(&mut state.test_data, event)
    }
}

impl Dispatch<wl_buffer::WlBuffer, ()> for WaylandTest {
    fn event(
        state: &mut Self,
        _proxy: &wl_buffer::WlBuffer,
        event: <wl_buffer::WlBuffer as wayland_client::Proxy>::Event,
        _data: &(),
        _conn: &wayland_client::Connection,
        _qhandle: &wayland_client::QueueHandle<Self>,
    ) {
        state
            .core_callbacks
            .buffer_callback(&mut state.test_data, event)
    }
}

impl Dispatch<wl_surface::WlSurface, ()> for WaylandTest {
    fn event(
        state: &mut Self,
        _proxy: &wl_surface::WlSurface,
        event: <wl_surface::WlSurface as wayland_client::Proxy>::Event,
        _data: &(),
        _conn: &wayland_client::Connection,
        _qhandle: &wayland_client::QueueHandle<Self>,
    ) {
        state
            .core_callbacks
            .surface_callback(&mut state.test_data, event)
    }
}

impl WaylandTest {
    pub fn create_buffer_raw(
        &mut self,
        offset: i32,
        width: i32,
        height: i32,
        stride: i32,
        format: wl_shm::Format,
    ) -> wl_buffer::WlBuffer {
        let shm = self.core_state.shm.as_ref().unwrap();
        let pool = self.core_state.shm_pool.get_or_insert_with(|| {
            let file =
                MemFile::create("wayland shm pool", CreateOptions::new().allow_sealing(true))
                    .unwrap();
            shm.create_pool(file.as_fd(), SHM_POOL_SIZE, &self.qh, ())
        });
        pool.create_buffer(offset, width, height, stride, format, &self.qh, ())
    }

    /// Create a buffer of the given width/height.
    pub fn create_buffer(&mut self, w: usize, h: usize) -> wl_buffer::WlBuffer {
        let shm = self.core_state.shm.as_ref().unwrap();
        let pool = self.core_state.shm_pool.get_or_insert_with(|| {
            let file =
                MemFile::create("wayland shm pool", CreateOptions::new().allow_sealing(true))
                    .unwrap();
            shm.create_pool(file.as_fd(), SHM_POOL_SIZE, &self.qh, ())
        });
        pool.create_buffer(
            0,
            w as i32,
            h as i32,
            w as i32 * 4,
            wl_shm::Format::Argb8888,
            &self.qh,
            (),
        )
    }

    /// Create a surface
    pub fn create_surface(&mut self) -> wl_surface::WlSurface {
        self.core_state
            .compositor
            .as_ref()
            .unwrap()
            .create_surface(&self.qh, ())
    }
}
