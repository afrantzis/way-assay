use std::{
    collections::HashMap,
    default::Default,
    env,
    path::PathBuf,
    rc::Rc,
    sync::{Arc, Condvar, Mutex},
    time::Duration,
};

use libtest_mimic::Failed;
use tracing::{instrument, trace};
use wayland_backend::protocol::ProtocolError;
use wayland_client::{
    globals::{self, GlobalList, GlobalListContents},
    protocol::{
        wl_buffer, wl_compositor, wl_display::WlDisplay, wl_registry, wl_shm, wl_shm_pool,
        wl_surface,
    },
    Connection, Dispatch, EventQueue, QueueHandle,
};
use wayland_protocols::xdg::shell::client::{xdg_surface, xdg_toplevel};

use crate::test_harness::Harness;

use self::{core::WaylandCoreState, stable::WaylandStableState};
mod core;
mod stable;

#[derive(Clone)]
pub struct Parameters {
    suite_path: PathBuf,
}

impl Parameters {
    pub fn new() -> anyhow::Result<Self> {
        let lib = env::var("WSSY_LIB")?;
        Ok(Parameters {
            suite_path: lib.into(),
        })
    }

    pub fn suite_path(&self) -> &PathBuf {
        &self.suite_path
    }
}

pub trait WaylandEventHandler: std::fmt::Debug {
    /// How long should we let a handler run
    fn timeout_ms(&self) -> Option<usize> {
        {
            None
        }
    }

    fn protocol_error_callback(&self, _test_data: &mut TestData, _error: ProtocolError) {}

    /// Callback for wl_registry global/remove event.
    ///
    /// Returns if the implementation handled this
    fn registry_callback(
        &self,
        _test_data: &mut TestData,
        _event: &<wl_registry::WlRegistry as wayland_client::Proxy>::Event,
    ) {
    }

    fn compositor_callback(
        &self,
        _test_data: &mut TestData,
        _event: <wl_compositor::WlCompositor as wayland_client::Proxy>::Event,
    ) {
    }

    fn shm_pool_callback(
        &self,
        _test_data: &mut TestData,
        _event: <wl_shm_pool::WlShmPool as wayland_client::Proxy>::Event,
    ) {
    }

    fn shm_callback(
        &self,
        _test_data: &mut TestData,
        _event: <wl_shm::WlShm as wayland_client::Proxy>::Event,
    ) {
    }
    fn buffer_callback(
        &self,
        _test_data: &mut TestData,
        _event: <wl_buffer::WlBuffer as wayland_client::Proxy>::Event,
    ) {
    }
    fn surface_callback(
        &self,
        _test_data: &mut TestData,
        _event: <wl_surface::WlSurface as wayland_client::Proxy>::Event,
    ) {
    }
}

#[derive(Debug)]
/// A ready-made error handlers for cases which shouldn't generate errors.
struct NoProtocolErrors;
impl WaylandEventHandler for NoProtocolErrors {
    fn protocol_error_callback(
        &self,
        test_data: &mut TestData,
        _error: wayland_backend::protocol::ProtocolError,
    ) {
        test_data.fail()
    }
}

#[derive(Debug, Default)]
/// Helper sync primitive for controlling tests and waiting on conditions.
struct WssySync<T: Clone, U: std::cmp::PartialEq> {
    /// Condition variable used for notication
    cvar: Condvar,

    /// Synchronization for test completion. Updated by the test thread (ie. typically just
    /// dispatch).
    ///
    /// bool: Whether the test is done.
    /// T: Data to be reported out at the end of the test
    /// Option<U> Condition set to wait on by the comparate
    termination: Mutex<(bool, T, Option<U>)>,

    /// When using the wait set of functions, this value will be compared to that passed into
    /// termination.2
    comparate: Option<U>,
}

impl<T, U> WssySync<T, U>
where
    T: Clone,
    U: std::cmp::PartialEq + std::fmt::Debug,
{
    #[allow(dead_code)]
    /// Wait for a test to finish and return the test's payload if successful.
    fn wait(&self) -> Result<T, Failed> {
        let timeout = Duration::from_millis(1000_u64);
        let lock = self.termination.lock().unwrap();
        let cvar = &self.cvar;
        let result = cvar
            .wait_timeout_while(lock, timeout, |(done, _serial, _)| !*done)
            .map_err(Failed::from)?;
        if result.1.timed_out() {
            Err(Failed::from("Timed out waiting for configure to be sent"))
        } else {
            Ok(result.0 .1.clone())
        }
    }

    /// Set data that can be extracted after a test has passed or failed.
    fn set_data(&mut self, data: T) {
        self.termination.lock().unwrap().1 = data;
    }

    /// Set the condition to wait for on the [`WssySync`]
    fn set_comparate(&mut self, comparate: Option<U>) {
        self.comparate = comparate;
    }

    /// Wait for the `comparate` to become equal to the condition set by [`set_comparate`]
    fn wait_compare(&mut self, comparate: &U, timeout: Option<Duration>) -> Result<T, Failed> {
        let timeout = timeout.unwrap_or(Duration::from_millis(250_u64));
        let lock = self.termination.lock().unwrap();
        let cvar = &self.cvar;
        let result = cvar
            .wait_timeout_while(lock, timeout, move |(done, _serial, comparer)| {
                !*done && (comparer.as_ref() == Some(comparate))
            })
            .map_err(Failed::from)?;
        if result.1.timed_out() {
            Err(Failed::from("Timed out waiting for configure to be sent"))
        } else {
            Ok(result.0 .1.clone())
        }
    }
}

#[derive(Debug)]
/// Data passed to callbacks and used for synchronization events
pub struct TestData {
    /// Sync variable to track when a test has passed or failed.
    test_done: WssySync<bool, bool>,

    /// Sync variable to track when an XDG surface's configure event was received.
    surface_configure: WssySync<u32, wayland_client::Weak<xdg_surface::XdgSurface>>,

    /// Sync variable to track when an XDG surface's toplevel event was received.
    toplevel_configure:
        WssySync<(i32, i32, Vec<u8>), wayland_client::Weak<xdg_toplevel::XdgToplevel>>,
}

type ProtocolCallbackMap = Rc<Mutex<HashMap<String, Box<dyn Fn()>>>>;

/// Abstraction for running Wayland protocol tests
pub struct WaylandTest {
    /// The display singleton.
    display: WlDisplay,

    /// Handle for the queue that requests are issued to.
    qh: QueueHandle<WaylandTest>,

    /// Callbacks for core events.
    core_callbacks: Arc<dyn WaylandEventHandler>,

    /// Data passed to individual tests and available from within the test.
    ///
    /// This structure is largely opaque to test writers. Generally the pass, fail, or wait
    /// functionality is all that is needed.
    test_data: TestData,

    /// Cached core state.
    ///
    /// This caches all global singletons as well as fundamental blocks for more complex operations
    /// with Wayland.
    core_state: WaylandCoreState,

    /// Cached stable state
    ///
    /// This caches all global singletons as well as fundamental blocks for Wayland's stable
    /// protocol.
    stable_state: WaylandStableState,

    /// Registered callbacks for protocol not handled by this test suite.
    cbs: ProtocolCallbackMap,
}

impl std::fmt::Debug for WaylandTest {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("WaylandTest")
            .field("display", &self.display)
            .field("qh", &self.qh)
            .field("core_callbacks", &self.core_callbacks)
            .field(
                "cbs",
                &self
                    .cbs
                    .try_lock()
                    .map(|hash| hash.keys().cloned().collect::<Vec<String>>()),
            )
            .field("test_data", &self.test_data)
            .field("core_state", &self.core_state)
            .field("stable_state", &self.stable_state)
            .finish()
    }
}

/// Main structure for managing WaylandTests
pub struct WaylandTestBuilder<H: Harness> {
    /// The external interface to communicating with a Wayland server.
    harness: H,

    /// A connection to a Wayland server. This must be established by the test.
    connection: Connection,

    /// All tests created from this builder.
    tests: Vec<WaylandTest>,

    /// Set of callbacks for core events.
    ///
    /// These callbacks are registered for all tests created by this builder.
    core_callbacks: Arc<dyn WaylandEventHandler>,
    //    cbs: HashMap<String, Box<dyn Fn(&WaylandTest, Box<dyn Any>)>>,
    /// Set of callbacks for external events not handled by this test suite.
    ///
    /// These callbacks are registered for all tests created by this builder.
    cbs: ProtocolCallbackMap,
}

impl Dispatch<wl_registry::WlRegistry, GlobalListContents> for WaylandTest {
    fn event(
        _state: &mut Self,
        _registry: &wl_registry::WlRegistry,
        _event: <wl_registry::WlRegistry as wayland_client::Proxy>::Event,
        _data: &GlobalListContents,
        _conn: &Connection,
        _qh: &QueueHandle<Self>,
    ) {
    }
}

impl Dispatch<wl_registry::WlRegistry, ()> for WaylandTest {
    #[instrument]
    fn event(
        state: &mut Self,
        registry: &wl_registry::WlRegistry,
        event: <wl_registry::WlRegistry as wayland_client::Proxy>::Event,
        _data: &(),
        _conn: &Connection,
        qh: &wayland_client::QueueHandle<Self>,
    ) {
        trace!("");
        match event {
            wl_registry::Event::Global {
                name,
                ref interface,
                version: _,
            } => {
                state.external_callback(interface);

                // These are fallbacks for WaylandTest usage. DefaultTest binds these in a
                // different way.
                let core_state = &mut state.core_state;
                match &interface[..] {
                    "wl_compositor" => {
                        let compositor = registry.bind(name, 5, qh, ());
                        core_state.compositor.replace(compositor);
                    }
                    "wl_shm" => {
                        let shm = registry.bind(name, 1, qh, ());
                        core_state.shm.replace(shm);
                    }
                    "wl_buffer" => {
                        let buffer = registry.bind(name, 1, qh, ());
                        core_state.buffer.replace(buffer);
                    }
                    "xdg_wm_base" => {
                        let wm_base = registry.bind(name, 6, qh, ());
                        state.stable_state.xdg_shell.wm_base.replace(wm_base);
                    }
                    _ => (),
                }
                // Call any callbacks for the WLRegistry global's appearance.
                state
                    .core_callbacks
                    .registry_callback(&mut state.test_data, &event)
            }
            wl_registry::Event::GlobalRemove { name: _ } => todo!(),
            _ => todo!(),
        }
    }
}

impl<H: Harness> WaylandTestBuilder<H> {
    /// Create a new "good" builder.
    ///
    /// This is a good builder because any tests created from this are expected to have no protocol
    /// errors. Additionally the tests created from this builder will not be able to override the
    /// handlers (although `add_external_handler` may be used.
    pub fn new(harness: H, connection: &Connection) -> WaylandTestBuilder<H> {
        Self {
            harness,
            connection: connection.clone(),
            tests: Vec::new(),
            core_callbacks: Arc::new(NoProtocolErrors),
            cbs: Rc::new(Mutex::new(HashMap::new())),
        }
    }

    /// Create a builder with a handler.
    pub fn new_with_handler<W>(
        harness: H,
        connection: &Connection,
        core_callbacks: W,
    ) -> WaylandTestBuilder<H>
    where
        W: WaylandEventHandler + 'static,
    {
        Self {
            harness,
            connection: connection.clone(),
            tests: Vec::new(),
            core_callbacks: Arc::new(core_callbacks),
            cbs: Rc::new(Mutex::new(HashMap::new())),
        }
    }

    /// Add a handler for external protocol.
    ///
    /// The prefix should match the protocol prefix, ie. xwg_ to get all XDG protocol
    pub fn add_external_handler<F: Fn() + 'static>(
        self,
        prefix: &str,
        f: F,
    ) -> WaylandTestBuilder<H> {
        self.cbs
            .lock()
            .unwrap()
            .insert(prefix.to_string(), Box::new(f));
        self
    }

    fn __create_test(
        &mut self,
        event_queue: &EventQueue<WaylandTest>,
        default_value: bool,
    ) -> WaylandTest {
        let display = self.connection.display();

        WaylandTest {
            display,
            qh: event_queue.handle(),
            core_callbacks: self.core_callbacks.clone(),
            cbs: self.cbs.clone(),
            test_data: TestData {
                test_done: WssySync {
                    cvar: Condvar::new(),
                    termination: Mutex::new((false, false, Some(default_value))),
                    comparate: None,
                },
                surface_configure: WssySync {
                    cvar: Condvar::new(),
                    termination: Mutex::new((false, 0, None)),
                    comparate: None,
                },
                toplevel_configure: WssySync {
                    cvar: Condvar::new(),
                    termination: Mutex::new((false, (-1, -1, vec![]), None)),
                    comparate: None,
                },
            },
            core_state: Default::default(),
            stable_state: Default::default(),
        }
    }

    /// Create a test that needs a certain goal to pass
    pub fn create_goal_test(&mut self, event_queue: &EventQueue<WaylandTest>) -> WaylandTest {
        self.__create_test(event_queue, false)
    }

    /// Create a test that passes unless a fail occurs.
    pub fn create_test(&mut self, event_queue: &EventQueue<WaylandTest>) -> WaylandTest {
        self.__create_test(event_queue, true)
    }
}

/// Helper for common test setup
pub struct DefaultTest {
    /// The Wayland client connection
    connection: Connection,

    /// queue for requests
    eq: EventQueue<WaylandTest>,

    /// A WaylandTest that is being defaulted.
    test: WaylandTest,
}

impl DefaultTest {
    /// Create a new DefaultTest with proper handling of core and stable protocol.
    pub fn new<H: Harness>(harness: H) -> Self {
        let connection = harness.connection().unwrap();
        let (globals, event_queue) =
            globals::registry_queue_init::<WaylandTest>(&connection).unwrap();
        let mut builder = WaylandTestBuilder::new(harness, &connection);
        let test = builder.create_test(&event_queue);
        Self {
            connection,
            eq: event_queue,
            test,
        }
        .bind_core_singletons(&globals)
        .bind_stable_singletons(&globals)
    }

    /// Issue a Wayland roundtrip. Effectively, diplay.sync + callback
    pub fn roundtrip(&mut self) {
        self.test.roundtrip(&mut self.eq)
    }

    /// Determine if the test has passed
    pub fn pass(&self, msg: &str) -> Result<(), Failed> {
        self.test.pass().then_some(()).ok_or(Failed::from(msg))
    }

    // TODO Write a delegate macro for these

    /// Return the current WlRegisty object
    pub fn get_registry(&self) -> wl_registry::WlRegistry {
        self.test.get_registry()
    }

    /// Create a WlBuffer of the given dimensions
    pub fn create_buffer(&mut self, w: usize, h: usize) -> wl_buffer::WlBuffer {
        self.test.create_buffer(w, h)
    }

    /// Creates a WlSurface.
    ///
    /// A buffer must be attached to a surface to be usable for display.
    pub fn create_surface(&mut self) -> wl_surface::WlSurface {
        self.test.create_surface()
    }

    /// Wait for this test to pass.
    pub fn wait_for_pass(&mut self) -> Result<(), Failed> {
        self.test.wait_for_pass()
    }

    fn bind_core_singletons(mut self, globals: &GlobalList) -> Self {
        let registry = globals.registry();
        let contents = globals.contents();
        let qh = &self.eq.handle();
        contents.with_list(|globals| {
            for global in globals.iter() {
                match &global.interface[..] {
                    "wl_compositor" => {
                        let compositor = registry.bind(global.name, global.version, qh, ());
                        self.test.core_state.compositor.replace(compositor);
                    }
                    "wl_shm" => {
                        let shm = registry.bind(global.name, global.version, qh, ());
                        self.test.core_state.shm.replace(shm);
                    }
                    _ => (),
                }
            }
        });
        self
    }
}

impl TestData {
    /// Mark the test as having passed.
    ///
    /// Generally this would be called by a Dispatch implementation or callback
    pub fn pass(&mut self) {
        let lock = &mut self.test_done.termination.lock().unwrap();
        let cvar = &self.test_done.cvar;
        **lock = (true, true, Some(true));
        cvar.notify_one();
    }

    /// Mark the test as having failed.
    ///
    /// Generally this would be called by a Dispatch implementation or callback
    pub fn fail(&mut self) {
        let lock = &mut self.test_done.termination.lock().unwrap();
        let cvar = &self.test_done.cvar;
        **lock = (true, false, Some(false));
        cvar.notify_one();
    }
}

impl WaylandTest {
    pub fn get_registry(&self) -> wl_registry::WlRegistry {
        self.display.get_registry(&self.qh, ())
    }

    pub fn roundtrip(&mut self, eq: &mut EventQueue<Self>) {
        let roundtrip_result = eq.roundtrip(self);
        let Err(e) = roundtrip_result else {
            return;
        };
        match e {
            wayland_client::DispatchError::BadMessage {
                sender_id: _,
                interface: _,
                opcode: _,
            } => todo!(),
            wayland_client::DispatchError::Backend(wayland_backend::client::WaylandError::Io(
                _io,
            )) => todo!(),
            wayland_client::DispatchError::Backend(
                wayland_backend::client::WaylandError::Protocol(protocol_error),
            ) => {
                self.core_callbacks
                    .protocol_error_callback(&mut self.test_data, protocol_error);
            }
        }
    }

    pub fn pass(&self) -> bool {
        self.test_data
            .test_done
            .termination
            .lock()
            .unwrap()
            .2
            .unwrap()
    }

    pub fn wait_for_pass(&mut self) -> Result<(), Failed> {
        self.test_data
            .test_done
            .wait_compare(&true, None)
            .map(|_| ())
    }

    pub fn external_callback(&self, global: &str) {
        let _ = self.cbs.lock().unwrap().get(global).map(|f| f());
    }
}
