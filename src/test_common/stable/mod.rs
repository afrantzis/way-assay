use wayland_client::globals::GlobalList;

use self::xdg::WaylandXdgState;

use super::DefaultTest;

mod xdg;
#[derive(Debug, Default)]
pub(crate) struct WaylandStableState {
    pub(crate) xdg_shell: WaylandXdgState,
}

impl DefaultTest {
    pub(crate) fn bind_stable_singletons(mut self, globals: &GlobalList) -> Self {
        let registry = globals.registry();
        let contents = globals.contents();
        let qh = &self.eq.handle();
        contents.with_list(|globals| {
            for global in globals.iter() {
                match &global.interface[..] {
                    "xdg_wm_base" => {
                        let wm_base = registry.bind(global.name, global.version, qh, ());
                        self.test.stable_state.xdg_shell.wm_base.replace(wm_base);
                    }
                    _ => (),
                }
            }
        });
        self
    }
}
