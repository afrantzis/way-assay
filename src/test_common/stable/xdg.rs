use libtest_mimic::Failed;
use wayland_client::{
    protocol::{wl_buffer, wl_surface},
    Dispatch, Proxy,
};
use wayland_protocols::xdg::shell::client::{
    xdg_surface::{self},
    xdg_toplevel::{self},
    xdg_wm_base,
};

use crate::test_common::{DefaultTest, TestData, WaylandTest};

#[derive(Debug, Default)]
pub struct WaylandXdgState {
    pub(crate) wm_base: Option<xdg_wm_base::XdgWmBase>,
}

impl Dispatch<xdg_wm_base::XdgWmBase, ()> for WaylandTest {
    fn event(
        _state: &mut Self,
        wm_base: &xdg_wm_base::XdgWmBase,
        event: <xdg_wm_base::XdgWmBase as wayland_client::Proxy>::Event,
        _data: &(),
        _conn: &wayland_client::Connection,
        _qhandle: &wayland_client::QueueHandle<Self>,
    ) {
        if let xdg_wm_base::Event::Ping { serial } = event {
            wm_base.pong(serial);
        }
    }
}

impl Dispatch<xdg_surface::XdgSurface, ()> for WaylandTest {
    fn event(
        state: &mut Self,
        xdg_surface: &xdg_surface::XdgSurface,
        event: <xdg_surface::XdgSurface as wayland_client::Proxy>::Event,
        _data: &(),
        _conn: &wayland_client::Connection,
        _qhandle: &wayland_client::QueueHandle<Self>,
    ) {
        if let xdg_surface::Event::Configure { serial, .. } = event {
            // Let the test handle the ack
            //xdg_surface.ack_configure(serial);
            state
                .test_data
                .ack_surface_configure(xdg_surface.downgrade(), serial)
        }
    }
}

impl Dispatch<xdg_toplevel::XdgToplevel, ()> for WaylandTest {
    fn event(
        state: &mut Self,
        xdg_toplevel: &xdg_toplevel::XdgToplevel,
        event: <xdg_toplevel::XdgToplevel as wayland_client::Proxy>::Event,
        _data: &(),
        _conn: &wayland_client::Connection,
        _qhandle: &wayland_client::QueueHandle<Self>,
    ) {
        match event {
            xdg_toplevel::Event::Configure {
                width,
                height,
                states,
            } => state.test_data.ack_toplevel_configure(
                xdg_toplevel.downgrade(),
                width,
                height,
                states,
            ),
            /*
                states.iter().for_each(|state| {
                let Ok(state) = xdg_toplevel::State::try_from(*state as u32) else {
                    return;
                };
                match state {
                    xdg_toplevel::State::Resizing => todo!(),
                    xdg_toplevel::State::Maximized => todo!(),
                    xdg_toplevel::State::Fullscreen => todo!(),
                    xdg_toplevel::State::Activated => todo!(),
                    xdg_toplevel::State::TiledLeft => todo!(),
                    xdg_toplevel::State::TiledRight => todo!(),
                    xdg_toplevel::State::TiledTop => todo!(),
                    xdg_toplevel::State::TiledBottom => todo!(),
                    xdg_toplevel::State::Suspended => todo!(),
                    _ => todo!(),
                }
            }),*/
            xdg_toplevel::Event::Close => (),
            xdg_toplevel::Event::ConfigureBounds {
                width: _,
                height: _,
            } => (),
            xdg_toplevel::Event::WmCapabilities { capabilities: _ } => (),
            _ => (),
        }
    }
}

impl TestData {
    pub fn ack_surface_configure(
        &mut self,
        xdg_surface: wayland_client::Weak<xdg_surface::XdgSurface>,
        data: u32,
    ) {
        self.surface_configure.set_data(data);
        self.surface_configure.set_comparate(Some(xdg_surface));
    }

    pub fn ack_toplevel_configure(
        &mut self,
        xdg_toplevel: wayland_client::Weak<xdg_toplevel::XdgToplevel>,
        width: i32,
        height: i32,
        states: Vec<u8>,
    ) {
        self.toplevel_configure.set_data((width, height, states));
        self.toplevel_configure.set_comparate(Some(xdg_toplevel));
    }
}

impl WaylandTest {
    pub fn wait_for_surface_configure(
        &mut self,
        xdg_surface: &xdg_surface::XdgSurface,
    ) -> Result<u32, Failed> {
        self.test_data
            .surface_configure
            .wait_compare(&xdg_surface.downgrade(), None)
    }

    pub fn wait_for_toplevel_configure(
        &mut self,
        xdg_toplevel: &xdg_toplevel::XdgToplevel,
    ) -> Result<(i32, i32, Vec<u8>), Failed> {
        self.test_data
            .toplevel_configure
            .wait_compare(&xdg_toplevel.downgrade(), None)
    }

    pub fn create_xdg_surface(
        &mut self,
        surface: &wl_surface::WlSurface,
    ) -> Result<(xdg_surface::XdgSurface, xdg_toplevel::XdgToplevel), Failed> {
        let xdg_surface = self
            .stable_state
            .xdg_shell
            .wm_base
            .as_ref()
            .unwrap()
            .get_xdg_surface(surface, &self.qh, ());
        let toplevel = xdg_surface.get_toplevel(&self.qh, ());
        Ok((xdg_surface, toplevel))
    }
}

pub struct WssyXdgSurface {
    surface: wl_surface::WlSurface,
    buffer: wl_buffer::WlBuffer,
    xdg_surface: xdg_surface::XdgSurface,
    toplevel: xdg_toplevel::XdgToplevel,
}

impl DefaultTest {
    pub fn create_xdg_surface(&mut self) -> Result<WssyXdgSurface, Failed> {
        let surface = self.create_surface();
        let buffer = self.create_buffer(200, 200);
        let (xdg_surface, toplevel) = self.test.create_xdg_surface(&surface)?;
        toplevel.set_title("default XDG surface".to_string());
        surface.commit();
        self.roundtrip();
        if let Ok(serial) = self.wait_for_surface_configure(&xdg_surface) {
            xdg_surface.ack_configure(serial);
        }

        // Wait for a toplevel
        if let Ok(_x) = self.wait_for_toplevel_configure(&toplevel) {}

        // Attach the buffer
        surface.attach(Some(&buffer), 0, 0);
        surface.commit();
        self.roundtrip();

        Ok(WssyXdgSurface {
            surface,
            buffer,
            xdg_surface,
            toplevel,
        })
    }

    pub fn wait_for_surface_configure(
        &mut self,
        xdg_surface: &xdg_surface::XdgSurface,
    ) -> Result<u32, Failed> {
        self.test.wait_for_surface_configure(xdg_surface)
    }

    pub fn wait_for_toplevel_configure(
        &mut self,
        xdg_toplevel: &xdg_toplevel::XdgToplevel,
    ) -> Result<(i32, i32, Vec<u8>), Failed> {
        self.test.wait_for_toplevel_configure(xdg_toplevel)
    }
}
