use wayland_client::Connection;

/// A Harness is the interface for Wayland test suite integration
///
/// The test harness represents the API that any Wayland test interface must implement in order to
/// be supported by this test infrastructure.
pub trait Harness: std::fmt::Debug {
    /// Get the backend of a Wayland client.
    ///
    /// The [Wayland client
    /// backend](https://smithay.github.io/smithay/wayland_backend/sys/client/struct.Backend.html)
    /// interacts with the client side backend ie. libwayland. Generally a test harness would
    /// create a client Wayland socket and obtain the backend from that.
    fn connection(&self) -> anyhow::Result<Connection>;

    /// Create a new Window
    ///
    /// Tell the Wayland implementation to create a new Window. What a window means is specific to
    /// the implementation.
    ///
    /// TODO: Add x, y, size, etc.
    fn create_window(&self, backend: wayland_client::backend::Backend);
}
