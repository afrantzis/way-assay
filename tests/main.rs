use std::sync::OnceLock;

use anyhow::Context;
use libtest_mimic::Arguments;

use tracing_subscriber::EnvFilter;
use way_assay::{test_common::Parameters, wlcs::WlcsConnection};

use crate::tests::{core, xdg::advanced as xdg_advanced, xdg::basic as xdg_basic};

mod tests;

fn setup_tracing() -> anyhow::Result<()> {
    let subscriber = tracing_subscriber::fmt()
        .pretty()
        .with_env_filter(EnvFilter::from_default_env())
        .with_test_writer()
        .finish();

    tracing::subscriber::set_global_default(subscriber)
        .context("Couldn't set up tracing subscriber")
}

static PARAMETERS: OnceLock<Parameters> = OnceLock::new();

fn main() {
    let args = Arguments::from_args();
    let mut tests = Vec::new();
    let params = PARAMETERS.get_or_init(|| {
        Parameters::new()
            .context("Couldn't initialize parameters")
            .unwrap()
    });

    #[cfg(feature = "wlcs")]
    let wlcs = WlcsConnection::new(&params.suite_path().to_string_lossy()).unwrap();

    // Start the fake wayland server's main loop
    #[cfg(feature = "wlcs")]
    wlcs.start();

    core::add_tests(&mut tests, wlcs.clone());
    xdg_basic::add_tests(&mut tests, wlcs.clone());
    xdg_advanced::add_tests(&mut tests, wlcs.clone());

    let _trace = setup_tracing();

    let conclusion = libtest_mimic::run(&args, tests);

    println!("{conclusion:?}");
    conclusion.exit_if_failed();
    wlcs.stop();
}
