use std::sync::atomic::{AtomicUsize, Ordering};

use libtest_mimic::{Failed, Trial};
use way_assay::test_harness::Harness;

use wayland_client::protocol::wl_registry;

use way_assay::test_common::{DefaultTest, TestData, WaylandEventHandler, WaylandTestBuilder};

use crate::push_test;

#[derive(Debug)]
struct CompositorTest;
#[derive(Debug)]
struct BadBuffer;

static COUNT: AtomicUsize = AtomicUsize::new(0);

impl WaylandEventHandler for CompositorTest {
    fn registry_callback(
        &self,
        test_data: &mut TestData,
        event: &<wl_registry::WlRegistry as wayland_client::Proxy>::Event,
    ) {
        if let wl_registry::Event::Global {
            name: _,
            interface,
            version: _,
        } = event
        {
            if interface.as_str() == "wl_compositor" && COUNT.fetch_add(1, Ordering::Relaxed) == 1 {
                test_data.pass();
            }
        }
    }
}

impl WaylandEventHandler for BadBuffer {
    fn protocol_error_callback(
        &self,
        test_data: &mut TestData,
        error: wayland_backend::protocol::ProtocolError,
    ) {
        if error.object_interface == "wl_shm_pool" && error.code == 1 {
            test_data.pass();
        }
    }
}

/// Tests that wl_compositor global is present, but do it... twice
fn wl_compositor_present<H: Harness>(harness: H) -> Result<(), Failed> {
    let connection = harness.connection().unwrap();
    let mut event_queue = connection.new_event_queue();
    let mut harness = WaylandTestBuilder::new_with_handler(harness, &connection, CompositorTest);
    let mut test = harness.create_goal_test(&event_queue);

    test.get_registry();
    test.roundtrip(&mut event_queue);

    // and do it again to make sure it's still there :)
    let mut test = harness.create_goal_test(&event_queue);
    test.get_registry();
    test.roundtrip(&mut event_queue);
    test.pass()
        .then_some(())
        .ok_or(Failed::from("wl_compositor wasn't found"))
    //    test.timed_wait()
}

fn bad_buffer_stride<H: Harness>(harness: H) -> Result<(), Failed> {
    let connection = harness.connection().unwrap();
    let mut event_queue = connection.new_event_queue();
    let mut harness = WaylandTestBuilder::new_with_handler(harness, &connection, BadBuffer);
    let mut test = harness.create_goal_test(&event_queue);
    test.get_registry();
    test.roundtrip(&mut event_queue);
    test.create_buffer_raw(
        0,
        1,
        2,
        3,
        wayland_client::protocol::wl_shm::Format::Argb8888,
    );
    test.roundtrip(&mut event_queue);
    test.wait_for_pass()
}

fn basic_buffer<H: Harness>(harness: H) -> Result<(), Failed> {
    let mut default_test = DefaultTest::new(harness);

    default_test.create_buffer(200, 200);
    default_test.roundtrip();
    default_test.pass("Couldn't create a basic buffer")
}

fn basic_surface<H: Harness>(harness: H) -> Result<(), Failed> {
    let mut default_test = DefaultTest::new(harness);

    let surface = default_test.create_surface();
    let buffer = default_test.create_buffer(200, 200);
    surface.attach(Some(&buffer), 0, 0);
    default_test.roundtrip();
    default_test.pass("Failed to create surface with buffer attachment")
}

pub fn add_tests<H: Harness + Send + Sync + Clone + 'static>(tests: &mut Vec<Trial>, harness: H) {
    push_test!(tests, harness, wl_compositor_present);
    push_test!(tests, harness, bad_buffer_stride);
    push_test!(tests, harness, basic_buffer);
    push_test!(tests, harness, basic_surface);
}
