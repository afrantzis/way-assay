pub mod core;
pub mod xdg;

#[macro_export]
macro_rules! push_test {
    ($tests:ident, $harness:ident, $fn: ident) => {
        let h = $harness.clone();
        $tests.push(Trial::test(stringify!($fn), move || $fn(h)));
    };
}
