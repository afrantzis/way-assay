use libtest_mimic::{Failed, Trial};
use way_assay::{
    test_common::{WaylandTest, WaylandTestBuilder},
    test_harness::Harness,
};
use wayland_client::EventQueue;

use crate::push_test;

fn xdg_manual_basic<H: Harness>(harness: H) -> Result<(), Failed> {
    let connection = harness.connection().unwrap();
    let mut event_queue: EventQueue<WaylandTest> = connection.new_event_queue();
    let mut builder = WaylandTestBuilder::new(harness, &connection)
        .add_external_handler("xdg_wm_base", || println!("huzzah"));
    let mut test = builder.create_goal_test(&event_queue);
    test.get_registry();
    test.roundtrip(&mut event_queue);
    Ok(())
}

pub fn add_tests<H: Harness + Send + Sync + Clone + 'static>(tests: &mut Vec<Trial>, harness: H) {
    push_test!(tests, harness, xdg_manual_basic);
}
