use libtest_mimic::{Failed, Trial};
use way_assay::{test_common::DefaultTest, test_harness::Harness};

use crate::push_test;

fn basic_xdg_toplevel<H: Harness>(harness: H) -> Result<(), Failed> {
    let mut default_test = DefaultTest::new(harness);
    default_test.create_xdg_surface().map(|_s| ())
}

pub fn add_tests<H: Harness + Send + Sync + Clone + 'static>(tests: &mut Vec<Trial>, harness: H) {
    push_test!(tests, harness, basic_xdg_toplevel);
}
